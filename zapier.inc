<?php
  // zapier.inc
  /**
   * Callback for creating subscription resources.
   *
   * @param object $data
   * @return object
   */
  function _zapier_subscription_create($data) {
    global $user;

    unset($data->id);
    $data->uid = $user->uid;
    $data->created = time();
    $data->modified = time();

    if (!isset($data->event)) {
      return services_error('Missing subscription attribute event', 406);
    }

    if (!isset($data->target_url)) {
      return services_error('Missing subscription attribute target_url', 406);
    }

    zapier_write_subscription($data);
    return (object)array(
      'id' => $data->id,
      'uri' => services_resource_uri(array('zapier_subscription', $data->id)),
    );
  }

  /**
   * Callback for updating subscription resources.
   *
   * @param int $id
   * @param object $data
   * @return object
   */
  function _zapier_subscription_update($id, $data) {
    global $user;
    $subscription = zapier_get_subscription($id);

    unset($data->created);
    $data->id = $id;
    $data->uid = $subscription->uid;
    $data->modified = time();

    zapier_write_subscription($data);
    return (object)array(
      'id' => $id,
      'uri' => services_resource_uri(array('zapier_subscription', $id)),
    );
  }

  /**
   * Callback for retrieving subscription resources.
   *
   * @param int $id
   * @return object
   */
  function _zapier_subscription_retrieve($id) {
    return zapier_get_subscription($id);
  }

  /**
   * Callback for deleting subscription resources.
   *
   * @param int $id
   * @return object
   */
  function _zapier_subscription_delete($id) {
    zapier_delete_subscription($id);
    return (object)array(
      'id' => $id,
    );
  }

  /**
   * Callback for listing subscriptions.
   *
   * @param int $page
   * @param array $parameters
   * @return array
   */
  function _zapier_subscription_index($page, $parameters) {
    global $user;

    $subscriptions = array();
    $res = db_query("SELECT * FROM {zapier_webhook_subscription} WHERE uid=:uid ORDER BY modified DESC", array(
      ':uid' => $user->uid,
    ));

    while ($subscription = db_fetch_object($res)) {
      $subscriptions[] = $subscription;
    }

    return $subscriptions;
  }
?>
