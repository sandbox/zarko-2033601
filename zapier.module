<?php
  // zapier.module
  /**
   * Implements hook_perm().
   */
  function zapier_permission() {
    return array(
      'zapier subscription admin' => array(
        'title' => t('Administer webhook subscriptions'),
        'description' => t('Administer all webhook subscriptions for all users.'),
      ),
      'zapier subscribe' => array(
        'title' => t('Subscribe to webhooks'),
        'description' => t('Subscribe/unsubscribe to webhooks.'),
      ),
    );
  }

  /**
   * Gets a subscription object by id.
   *
   * @param int $id
   * @return object
   */
  function zapier_get_subscription($id) {
    return db_fetch_object(db_query("SELECT * FROM {zapier_webhook_subscription} WHERE id=:id", array(
      ':id' => $id,
    )));
  }

  /**
   * Writes a subscription to the database
   *
   * @param object $subscription
   * @return void
   */
  function zapier_write_subscription($subscription) {
    $primary_key = !empty($subscription->id) ? array('id') : NULL;
    drupal_write_record('zapier_webhook_subscription', $subscription, $primary_key);
  }

  /**
   * Deletes a subscription from the database.
   *
   * @param int $id
   * @return void
   */
  function zapier_delete_subscription($id) {
    db_query("DELETE FROM {zapier_webhook_subscription} WHERE id=:id", array(
      ':id' => $id,
    ));
  }

  /**
   * Implements hook_services_resources().
   */
  function zapier_services_resources() {
    return array(
      'hooks' => array(
        'retrieve' => array(
          'help' => 'Retrieves a subscription',
          'file' => array('file' => 'inc', 'module' => 'zapier'),
          'callback' => '_zapier_subscription_retrieve',
          'access callback' => '_zapier_access',
          'access arguments' => array('view'),
          'access arguments append' => TRUE,
          'args' => array(
            array(
              'name' => 'id',
              'type' => 'int',
              'description' => 'The id of the subscription to get',
              'source' => array('path' => '0'),
              'optional' => FALSE,
            ),
          ),
        ),
        'create' => array(
          'help' => 'Creates a subscription',
          'file' => array('file' => 'inc', 'module' => 'zapier'),
          'callback' => '_zapier_subscription_create',
          'access arguments' => array('zapier subscribe'),
          'access arguments append' => FALSE,
          'args' => array(
            array(
              'name' => 'data',
              'type' => 'struct',
              'description' => 'The subscription object',
              'source' => 'data',
              'optional' => FALSE,
            ),
          ),
        ),
        'update' => array(
          'help' => 'Updates a subscription',
          'file' => array('file' => 'inc', 'module' => 'zapier'),
          'callback' => '_zapier_subscription_update',
          'access callback' => '_zapier_access',
          'access arguments' => array('update'),
          'access arguments append' => TRUE,
          'args' => array(
            array(
              'name' => 'id',
              'type' => 'int',
              'description' => 'The id of the node to update',
              'source' => array('path' => '0'),
              'optional' => FALSE,
            ),
            array(
              'name' => 'data',
              'type' => 'struct',
              'description' => 'The subscription data object',
              'source' => 'data',
              'optional' => FALSE,
            ),
          ),
        ),
        'delete' => array(
          'help' => 'Deletes a subscription',
          'file' => array('file' => 'inc', 'module' => 'zapier'),
          'callback' => '_zapier_subscription_delete',
          'access callback' => '_zapier_access',
          'access arguments' => array('delete'),
          'access arguments append' => TRUE,
          'args' => array(
            array(
              'name' => 'nid',
              'type' => 'int',
              'description' => 'The id of the subscription to delete',
              'source' => array('path' => '0'),
              'optional' => FALSE,
            ),
          ),
        ),
        'index' => array(
          'help' => 'Retrieves a listing of subscriptions',
          'file' => array('file' => 'inc', 'module' => 'zapier'),
          'callback' => '_zapier_subscription_index',
          'access callback' => 'user_access',
          'access arguments' => array('access content'),
          'access arguments append' => FALSE,
          'args' => array(
            array(
              'name' => 'page',
              'type' => 'int',
              'description' => '',
              'source' => array(
                'param' => 'page',
              ),
              'optional' => TRUE,
              'default value' => 0,
            ),
            array(
              'name' => 'parameters',
              'type' => 'array',
              'description' => '',
              'source' => 'param',
              'optional' => TRUE,
              'default value' => array(),
            ),
          ),
        ),
      ),
    );
  }

  /**
   * Access callback for the note resource.
   *
   * @param string $op
   *  The operation that's going to be performed.
   * @param array $args
   *  The arguments that will be passed to the callback.
   * @return bool
   *  Whether access is given or not.
   */
  function _zapier_access($op, $args) {
    global $user;
    $access = FALSE;

    switch ($op) {
      case 'view':
        $subscription = zapier_get_subscription($args[0]);
        $access = user_access('zapier subscription admin');
        $access = $access || $subscription->uid == $user->uid && user_access('zapier subscribe');
        break;
      case 'update':
        $subscription = zapier_get_subscription($args[0]->id);
        $access = user_access('zapier subscription admin');
        $access = $access || $subscription->uid == $user->uid && user_access('zapier subscribe');
        break;
      case 'delete':
        $subscription = zapier_get_subscription($args[0]);
        $access = user_access('zapier subscription admin');
        $access = $access || $subscription->uid == $user->uid && user_access('zapier subscribe');
        break;
    }

    return $access;
  }
?>
